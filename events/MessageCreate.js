const { Events } = require('discord.js');
const { apikey } = require('../config.json');
const { google } = require('googleapis');
const fs = require('fs');
const path = require('node:path');
const regex = /(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=))([\w-]{11})/i;
let blacklist = [];

module.exports = {
	name: Events.MessageCreate,
	async execute(message) {
		console.log(`${message.author.username} posted ${message.content} in ${message.channel.name}`);
        const { youtube } = google.youtube({
            version: 'v3',
            auth: apikey
        });
        if (message.content.includes("youtu")) {
            console.log("A youtube link has been posted");
            var match = regex.exec(message.content);
            if (match) {
                console.log(`The video ID is ${match[1]}`);
                try {
                    let compactname = message.guild.name.replace(/\s/g, '');
                    if (fs.existsSync(`./blacklist_${compactname}.json`)) {
                    blacklist = fs.readFileSync(`./blacklist_${compactname}.json`, 'utf-8');
                    } else {
                        return;
                        console.log("No blacklist file detected");
                    }
                    const videoapiUrl = `https://www.googleapis.com/youtube/v3/videos?id=${match[1]}&key=${apikey}&part=snippet`;
                    const videoresponse = await fetch(videoapiUrl);
                    const videodata = await videoresponse.json();
                    var channelId = videodata.items[0].snippet.channelId;
                    console.log(`The channel ID is: ${channelId}`);
                    const channelapiUrl = `https://www.googleapis.com/youtube/v3/channels?id=${channelId}&key=${apikey}&part=snippet`;
                    const channelresponse = await fetch(channelapiUrl);
                    const channeldata = await channelresponse.json();
                    var channeltitle = channeldata.items[0].snippet.localized.title;
                    console.log(`The channel title is: ${channeltitle}`);
                    if (blacklist.includes(channeltitle)) {
                        message.delete();
                        console.log("Blacklisted channel detected, message deleted");
                        message.channel.send(`${message.author.username} posted a link to the blacklisted channel ${channeltitle}`);
                    }
                    return channeltitle;
                  } catch (error) {
                    console.error(`Error fetching video details: ${error}`);
                    return null;
                }
            } else {
                console.log("The posted message contained the string 'youtu', but no video ID match was found");
                return null;
            }
        }
	},
};
