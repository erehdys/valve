const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');
const path = require('node:path');
const { PermissionsBitField } = require('discord.js');
let blacklist = [];

module.exports = {
	data: new SlashCommandBuilder()
		.setName('addtoblacklist')
		.setDescription('Adds a channel to the black list')
		.addStringOption(option => option 
			.setName('channel')
			.setDescription('The channel that is going to be added to the blacklist')
		),
	async execute(interaction) {
		if ( !interaction.memberPermissions.has(PermissionsBitField.Flags.ManageMessages)) {
			console.log("You don't have the appropriate permissions to use this command");
			await interaction.reply(`You don't have the appropriate permissions to add new channels to the blacklist`);
			return;
		}
		const addedchannel = interaction.options.getString('channel');
		let compactname = interaction.guild.name.replace(/\s/g, '');
		if (fs.existsSync(`./blacklist_${compactname}.json`)) {
			console.log("Blacklist file for this server detected");
		} else {
			console.log("No blacklist file detected");
			let stringifiedplaceholder = JSON.stringify([]);
			fs.writeFileSync(`./blacklist_${compactname}.json`, stringifiedplaceholder, 'utf-8');
		}
		const currentserverblacklist = fs.readFileSync(`./blacklist_${compactname}.json`);
		blacklist = JSON.parse(currentserverblacklist);
		if (addedchannel != null) {
			blacklist.push(addedchannel);
			const stringifiedlist = JSON.stringify(blacklist);
			fs.writeFileSync(`./blacklist_${compactname}.json`, stringifiedlist, 'utf-8');
			await interaction.reply(`Added "${addedchannel}" to the channel blacklist`);
			console.log(blacklist);
		} else {
			await interaction.reply(`You did not specify a channel to be blacklisted`);
		}
	},
};
