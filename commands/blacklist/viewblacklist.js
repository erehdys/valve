const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');
const path = require('node:path');
let blacklist = [];

module.exports = {
	data: new SlashCommandBuilder()
		.setName('viewblacklist')
		.setDescription('Display the list of banned YouTube channels'),
	async execute(interaction) {
		let compactname = interaction.guild.name.replace(/\s/g, '');
		if (fs.existsSync(`./blacklist_${compactname}.json`)) {
			console.log("Blacklist file for this server detected");
		} else {
			console.log("No blacklist file detected");
			await interaction.reply(`This discord server does not have a channel blacklist yet`);
			return;
		}
		blacklist = fs.readFileSync(`./blacklist_${compactname}.json`, 'utf-8');
		// interaction.user is the object representing the User who ran the command
		// interaction.member is the GuildMember object, which represents the user in the specific guild
		await interaction.reply(`The following channels are in the blacklist: ${blacklist}`);
        console.log(blacklist);
		console.log(`./blacklist_${compactname}.json`);
	},
};
